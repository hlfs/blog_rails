module ApplicationHelper

  def header_bg(page)
    page
  end

  def header_title(page)
    case page
    when "about"
      "About Me"
    when "contact"
      "Contact Me"
    when "post"
      "Posts List"
    else
      "iPtcc Blog"
    end
  end

  def header_content(page)
    case page
    when "about"
      "This is what i do."
    when "contact"
      "Have questions? I hava answers (maybe)."
    when "post"
      "These are my posts."
    else
      "A Blog Demo by Ruby on Rails"
    end
  end

end
