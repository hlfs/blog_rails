json.array!(@lookups) do |lookup|
  json.extract! lookup, :id, :name, :code, :type, :position
  json.url lookup_url(lookup, format: :json)
end
