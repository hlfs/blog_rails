json.array!(@tags) do |tag|
  json.extract! tag, :id, :name, :frequency
  json.url tag_url(tag, format: :json)
end
