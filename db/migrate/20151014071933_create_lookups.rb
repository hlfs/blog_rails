class CreateLookups < ActiveRecord::Migration
  def change
    create_table :lookups do |t|
      t.string :name
      t.integer :code
      t.string :type
      t.string :position

      t.timestamps null: false
    end
  end
end
