class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content
      t.integer :status
      t.string :author
      t.string :email
      t.string :url
      t.references :post, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
